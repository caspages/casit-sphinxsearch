###
# Introduction
This docker file is build up to run the Sphinx search engine. For more info, please visit the Sphinx site below.

## Sphinx Project
[![Sphinx](http://sphinxsearch.com/images/logo.png)](http://sphinxsearch.com/)

# Configurations
You can either launch the docker in your local docker instance or put it into your docker swarm.
## Docker Compose
Copy or edit configurations below to use in your environment.
```
version: "3.5"

services:
  engine:
    image: casit/sphinxsearch:3.4.1
    ports:
      - 9306:9306
      - 9312:9312
    volumes:
      - ./index:/opt/sphinx/index
      - ./sphinx.conf:/opt/sphinx/conf/sphinx.conf
    deploy:
      resources:
        limits:
          cpus: '0.5'
          memory: 1024M
        reservations:
          cpus: '0.05'
          memory: 512M # match indexer.value from sphinx.conf
```

# Commands
## Startup
- Edit the file `sphinx.conf` and fill up database credentials, eg. host, user name, password, etc. More info: [sphinx.conf options reference](http://sphinxsearch.com/docs/current/conf-reference.html)
- Create a directory : `mkdir index`.
- Run the docker container `docker compose up -d`.

## To Re-Index
Do this every time you change the `sphinx.conf` file.
- `docker exec -it container_id bash` into the docker container and run command: `indexer --rotate --all --config /opt/sphinx/conf/sphinx.conf`
