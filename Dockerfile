# Dockerfile for Sphinx SE

FROM alpine:3.17 

# install dependencies
RUN  apk add mariadb-dev \
	unixodbc-dev \
	wget \
	&& apk cache clean

# set up and expose directories
RUN mkdir -pv /opt/sphinx/log /opt/sphinx/index /opt/sphinx/data /opt/sphinx/conf
VOLUME /opt/sphinx/index

# install Sphinx SE
# https://sphinxsearch.com/files/sphinx-3.4.1-efbcc65-linux-amd64-musl.tar.gz
RUN wget https://sphinxsearch.com/files/sphinx-3.4.1-efbcc65-linux-amd64-musl.tar.gz -O /tmp/sphinxsearch.tar.gz \
	&& cd /opt/sphinx \
	&& tar -xzvf /tmp/sphinxsearch.tar.gz \
	&& rm /tmp/sphinxsearch.tar.gz

# point to sphinx binaries
ENV PATH="${PATH}:/opt/sphinx/sphinx-3.4.1/bin"
# ENV LD_LIBRARY_PATH="/usr/lib64/mysql:$LD_LIBRARY_PATH"
RUN indexer -v

# redirect logs to stdout
RUN ln -sv /dev/stdout /opt/sphinx/log/query.log \
	&& ln -sv /dev/stdout /opt/sphinx/log/searchd.log

# copy the default/simple configuration
COPY sphinx.conf /opt/sphinx/conf/

# expose TCP ports
EXPOSE 9306
EXPOSE 9312

VOLUME /opt/sphinx/conf

# start the search demon
CMD ["/bin/sh", "-c", "searchd --nodetach --config /opt/sphinx/conf/sphinx.conf"]
